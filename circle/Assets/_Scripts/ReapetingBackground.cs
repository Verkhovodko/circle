﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReapetingBackground : MonoBehaviour
{

	private BoxCollider2D boundaryTopBottom;
	//private BoxCollider2D boundaryBottom;
	private float boundaryOffsetRate;

	// Use this for initialization
	void Start()
	{
		boundaryTopBottom = GetComponent<BoxCollider2D>();
		boundaryOffsetRate = 24.35f;
	}

	// Update is called once per frame
	void Update()
	{
		if (transform.position.x < -boundaryOffsetRate)
		{
			RepositionBackground();
		}
	}

	private void RepositionBackground()
	{
		Vector2 boundaryOffset = new Vector2(boundaryOffsetRate * 2f, 0);
		transform.position = (Vector2) transform.position + boundaryOffset;
	}
}
