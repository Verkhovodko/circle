﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

	public static GameController instance;
	
	public GameObject gameOverText;
	public bool gameOver;
	public float scrollSpeed = -1.5f;
	
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}
		
	}

	void Start ()
	{
		gameOver = false;
	}
	
	void Update () 
	{
		if (gameOver && Input.GetKeyDown(KeyCode.Space))
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
		
	}

	public void PlayerDied()
	{
		gameOverText.SetActive(true);
		gameOver = true;
	}
}
