﻿using System.Collections;
using System.Collections.Generic;
using System.Security;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[SerializeField] GameObject Player;
	[SerializeField] float verticalSpeed = 100;
	private bool isDead;

	// Use this for initialization
	void Start ()
	{
		isDead = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		var y = Input.GetAxis("Vertical") * Time.deltaTime * verticalSpeed;
		
		transform.Translate(0, y, 0);
		
	}

	void OnTriggerExit(Collider other)
	{
		Debug.Log("EXIT");
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (!gameObject.CompareTag("Boundary"))
		{
			isDead = true;
			GameController.instance.PlayerDied();	
		}
		
	}
}
